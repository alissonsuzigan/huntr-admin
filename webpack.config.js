const path = require('path');
// Global paths
const buildPath = path.resolve(process.cwd(), 'dist');
const configPath = path.resolve(process.cwd(), 'config');
const sourcePath = path.resolve(process.cwd(), 'src');
// Loaders and plugins
const autoprefixer = require('autoprefixer');
const CopyFile = require('copy-webpack-plugin');
const ExtractCSS = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const StyleLint = require('stylelint-webpack-plugin');
const webpack = require('webpack');
const env = process.env.NODE_ENV;


module.exports = {
  context: sourcePath,
  devtool: env === 'dev' ? 'eval' : 'source-map',

  entry: {
    app: './index.jsx',
    vendor: [
      'moment',
      'prop-types',
      'react',
      'react-dom',
      'react-redux',
      'react-router-dom',
      'redux',
      'redux-promise',
      'redux-thunk'
    ]
  },

  output: {
    filename: './static/js/[chunkhash:8].[name].js',
    // filename: './static/js/[name].js',
    path: buildPath
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      base: `${sourcePath}/styles/_base`,
      mixins: `${sourcePath}/styles/_mixins`,
      placeholder: `${sourcePath}/styles/_placeholder`
    },
    modules: [
      sourcePath,
      'node_modules'
    ],
  },


  // webpack-dev-server :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  devServer: {
    contentBase: sourcePath,
    compress: true,
    port: 8888,
    stats: {
      assets: true,
      children: false,
      chunks: false,
      modules: false
    }
  },

  // modules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  module: {
    rules: [
      { // eslint config
        test: /\.jsx?$/,
        exclude: /node_modules/,
        enforce: "pre",
        loader: "eslint-loader"
      },
      { // babel config
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ]
      },
      { // scss config
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractCSS.extract({
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer]
              }
            },
            'sass-loader',
          ]
        })
      },
      {
        test: /\.(pne?g|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: './static/img/[name].[ext]'
            }
          }
        ]
      }

    ]
  },

  // Plugins ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  plugins: [
    // Copy config json
    new CopyFile([
      { from: `${sourcePath}/keycloak.json` }
    ]),

    // Extract css
    // new ExtractCSS('./static/css/app.css'),
    new ExtractCSS('./static/css/[contenthash:8].[name].css'),

    // Generate HTML file
    new HtmlPlugin({
      template: 'index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),

    new StyleLint({
      files: '**/*.scss',
      syntax: 'scss'
    }),

    // Render application as production
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    // Generates vendor script separeted form app
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),

    // Minify assets
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),

    // Set momentjs to use specific locale: pt-br (and default en-US)
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /pt-br/)
  ]

};
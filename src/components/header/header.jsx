import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
// import { connect } from 'react-redux';
import logo from './walmart.svg';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Header(props) {
  const { data } = props;

  const renderItems = item => (
    <li className="header-menu-item" key={item.label}>
      <NavLink to={item.url} className="header-menu-link" activeClassName="active" title={item.label}>
        {item.label}
      </NavLink>
    </li>
  );

  return (
    <header id="header" className="header">
      <div className="header-title">
        <svg className="walmart-logo">
          <use xlinkHref={`${logo}#walmart-logo`} />
        </svg>

        <h1 className="title">
          <NavLink to={data.title.url} className="title-link" title={data.title.label}>
            {data.title.label}
          </NavLink>
        </h1>
      </div>

      {props.keycloak && props.keycloak.hasPermission &&
        <nav className="header-menu-nav">
          <ul className="header-menu-list">
            {data.menu.map(item => renderItems(item))}
          </ul>
        </nav>
      }
    </header>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Header.defaultProps = {
  keycloak: {
    hasPermission: true
  }
};

Header.propTypes = {
  data: PropTypes.shape({}).isRequired,
  keycloak: PropTypes.shape()
};

// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// const mapStateToProps = ({ keycloak }) => ({ keycloak });
// export default connect(mapStateToProps)(Header);
// export { Header };

export default Header;

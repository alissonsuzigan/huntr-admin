import React from 'react';
import Header from './header';
import headerData from '../../data/header-data.json';

const keycloak = {
  hasPermission: true
}

describe('<Header />', () => {
  const wrapper = shallow(<Header data={headerData} keycloak={keycloak} />);

  it('should verify header structure', () => {
    expect(wrapper.find('header')).toHaveLength(1);
    expect(wrapper.hasClass('header')).toBeTruthy();
    expect(wrapper.children()).toHaveLength(2);
  });


  it('should verify header-title structure', () => {
    const headerTitle = wrapper.childAt(0);
    expect(headerTitle.hasClass('header-title')).toBeTruthy();
    expect(wrapper.find('svg.walmart-logo')).toHaveLength(1);
    expect(wrapper.find('h1.title')).toHaveLength(1);
    expect(wrapper.find('NavLink.title-link')).toHaveLength(1);
  });


  it('should verify header-menu-nav structure', () => {
    const headerNav = wrapper.childAt(1);
    expect(headerNav.hasClass('header-menu-nav')).toBeTruthy();
    expect(headerNav.childAt(0).hasClass('header-menu-list')).toBeTruthy();
  });


  it('should verify header-menu-items structure', () => {
    const menuItem = wrapper.find('.header-menu-list').childAt(0);
    const menuLink = menuItem.childAt(0);
    expect(menuItem.hasClass('header-menu-item')).toBeTruthy();
    expect(menuLink.hasClass('header-menu-link')).toBeTruthy();
    expect(menuLink.prop('activeClassName')).toMatch('active');
  });

});
import { connect } from 'react-redux';
import Header from './header';

const mapStateToProps = ({ keycloak }) => ({ keycloak });
export default connect(mapStateToProps, null, null, { pure: false })(Header);

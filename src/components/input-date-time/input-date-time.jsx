import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Moment and DateTime
import moment from 'moment';
import DateTime from '../date-time';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class InputDateTime extends Component {
  constructor(props) {
    super();
    this.state = {
      // if props.value is '' use undefined as moment parameter
      moment: moment(props.value || undefined).locale('pt-br'),
      visible: false
    };
    this.setComponentRef = this.setComponentRef.bind(this);
    this.onClickOut = this.onClickOut.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onClear = this.onClear.bind(this);
    this.showComponent = this.showComponent.bind(this);
    this.hideComponent = this.hideComponent.bind(this);
  }

  // Lifecicle methods ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  componentDidMount() {
    document.addEventListener('click', this.onClickOut);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ moment: moment(nextProps.value || undefined).locale('pt-br') });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.onClickOut);
  }

  // Manipulation methods :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  onClickOut(event) {
    if (this.componentRef && !this.componentRef.contains(event.target) && this.state.visible) {
      this.hideComponent();
    }
  }

  onChange(value) {
    this.setState({ moment: value });
  }

  onSave() {
    const time = this.state.moment.valueOf();
    this.updateValue(time);
    this.hideComponent();
  }

  onClear() {
    this.updateValue('');
    this.hideComponent();
  }

  setComponentRef(node) {
    this.componentRef = node;
  }

  updateValue(value) {
    this.props.onChange(this.props.id, value);
  }

  // Display methods ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  showComponent() {
    this.setState({ visible: true });
  }

  hideComponent() {
    this.setState({ visible: false });
  }

  // Render :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  render() {
    const { id, label, value } = this.props;
    const inputValue = value === '' ? '' : moment(value).locale('pt-br').format('LLL');

    return (
      <div className="form-group input-date-time" ref={this.setComponentRef}>
        {label && <label htmlFor={id}>{`${label}:`}</label>}

        <input
          id={id}
          type="text"
          className="input"
          onFocus={this.showComponent}
          value={inputValue}
        />

        {this.state.visible &&
          <DateTime
            id={id}
            moment={this.state.moment}
            onChange={this.onChange}
            onSave={this.onSave}
            onClear={this.onClear}
          />
        }
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
InputDateTime.defaultProps = {
  label: ''
};

InputDateTime.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default InputDateTime;

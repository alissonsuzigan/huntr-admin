import React from 'react';
import PropTypes from 'prop-types';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Table({ caption, className, headerData, tbodyData, tbodyTemplate, serverTime }) {
  const TBodyTemplate = tbodyTemplate;
  const renderHeader = () => (
    <thead>
      <tr>
        {headerData.map((item, idx) => <th key={idx.toString()}>{item}</th>)}
      </tr>
    </thead>
  );

  return (
    <div>
      <table className={`table ${className}`}>
        {caption && <caption>{caption}</caption>}
        {headerData.length > 0 && renderHeader()}
        <TBodyTemplate tbodyData={tbodyData} serverTime={serverTime} />
      </table>
    </div>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Table.defaultProps = {
  caption: '',
  className: ''
};

Table.propTypes = {
  caption: PropTypes.string,
  className: PropTypes.string,
  headerData: PropTypes.arrayOf(PropTypes.string).isRequired,
  serverTime: PropTypes.number.isRequired,
  tbodyData: PropTypes.shape().isRequired,
  tbodyTemplate: PropTypes.func.isRequired
};

export default Table;

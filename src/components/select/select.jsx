import React from 'react';
import PropTypes from 'prop-types';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Select({ id, label, dataOptions, onChange, value, required }) {
  const renderOptions = option => (
    <option key={option.value} value={option.value.toString()}>
      {option.label}
    </option>
  );

  return (
    <div className="form-group">
      {label && <label htmlFor={id}>{`${label}:`}</label>}
      <select
        id={id}
        onChange={onChange}
        required={required}
        value={value}
      >
        <option value="" disabled>Selecionar</option>
        {dataOptions.map(renderOptions)}
      </select>
    </div>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Select.defaultProps = {
  label: '',
  value: '',
  required: false
};

Select.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  dataOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  required: PropTypes.bool
};

export default Select;


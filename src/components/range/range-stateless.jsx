import React from 'react';
import PropTypes from 'prop-types';
import Input from '../input';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Range({ id, label, min, max, step, value, onChange, required }) {
  const activePercent = (((value - min) * 100) / (max - min));
  const activeStyle = { backgroundSize: `${activePercent}% 100%` };

  return (
    <div className="form-group">
      {label && <label htmlFor={id}>{`${label}:`}</label>}
      <input
        id={id}
        className="range"
        type="range"
        min={min}
        max={max}
        step={step}
        value={value}
        onChange={onChange}
        required={required}
        style={activeStyle}
      />
      <Input className="range-output" value={value} readOnly onlyInput />
    </div>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Range.defaultProps = {
  label: '',
  min: '0',
  max: '100',
  step: '10',
  required: false
};

Range.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
  step: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool
};

export default Range;

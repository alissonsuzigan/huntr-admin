import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../input';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class Range extends PureComponent {
  constructor(props) {
    super();
    this.state = {
      value: props.value,
      updateTimeout: null
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps({ value }) {
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  onChange(event) {
    const { value } = event.target;
    this.setState({ value });

    if (this.state.updateTimeout) {
      clearTimeout(this.state.updateTimeout);
    }

    this.setState({
      updateTimeout: setTimeout(() => {
        this.props.onChange(this.props.id, value);
        this.setState({ updateTimeout: null });
      }, 300)
    });
  }

  render() {
    const { id, label, min, max, step, required } = this.props;
    const { value } = this.state;
    const activePercent = (((value - min) * 100) / (max - min));
    const activeStyle = { backgroundSize: `${activePercent}% 100%` };

    return (
      <div className="form-group">
        {label && <label htmlFor={id}>{`${label}:`}</label>}
        <input
          id={id}
          className="range"
          type="range"
          min={min}
          max={max}
          step={step}
          value={value}
          onChange={this.onChange}
          required={required}
          style={activeStyle}
        />
        <Input className="range-output" value={value} readOnly onlyInput />
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Range.defaultProps = {
  label: '',
  min: '0',
  max: '100',
  step: '10',
  required: false
};

Range.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
  step: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool
};

export default Range;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Icons
import MdClose from 'react-icons/lib/md/close';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class InputTag extends Component {
  constructor(props) {
    super();
    this.state = {
      value: '',
      tagList: props.value
    };
    this.onBlur = this.onBlur.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.onChangeValue = this.onChangeValue.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ tagList: nextProps.value });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.tagList !== this.state.tagList) {
      this.props.onChange(this.props.id, this.state.tagList);
    }
  }

  // Event methods ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  onChangeValue(event) {
    this.setState({ value: event.target.value });
  }

  onKeyPress(event) {
    const value = event.target.value.trim();
    if (event.key === 'Enter' && value) {
      event.preventDefault();
      this.addTag(value);
    }
  }

  onBlur(event) {
    const value = event.target.value.trim();
    if (value) {
      event.target.focus();
      this.addTag(value);
    }
  }

  onRemoveTag(event, tagId) {
    event.preventDefault();
    const listTag = this.state.tagList.filter((item, id) => id !== tagId);
    this.setState({ tagList: listTag });
  }

  addTag(value) {
    this.setState({
      tagList: [...this.state.tagList, value],
      value: ''
    });
  }

  // Render methods :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  renderTag() {
    if (this.state.tagList.length === 0) {
      return null;
    }
    const tags = this.state.tagList.map((value, tagId) => (
      <span className="tag-item" key={tagId.toString()}>
        <span className="tag-label">{value}</span>
        <button
          className="tag-remove"
          title="Remover tag"
          type="button"
          onClick={event => this.onRemoveTag(event, tagId)}
        >
          <MdClose />
        </button>
      </span>
    ));
    return tags;
  }

  render() {
    const { id, label, placeholder } = this.props;
    return (
      <div className="form-group">
        {label && <label htmlFor={id}>{`${label}:`}</label>}
        <div className="tag-container">
          {this.renderTag()}
          <input
            id={id}
            className="tag-input"
            value={this.state.value}
            onChange={this.onChangeValue}
            onKeyPress={this.onKeyPress}
            onBlur={this.onBlur}
            placeholder={placeholder}
          />
        </div>
      </div>
    );
  }
}

// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
InputTag.defaultProps = {
  id: '',
  label: '',
  placeholder: ''
};

InputTag.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired
};

export default InputTag;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


class Input extends PureComponent {
  constructor(props) {
    super();
    this.state = {
      value: props.value,
      updateTimeout: null
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps({ value }) {
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  onChange(event) {
    const { value } = event.target;
    this.setState({ value });

    if (this.state.updateTimeout) {
      clearTimeout(this.state.updateTimeout);
    }

    this.setState({
      updateTimeout: setTimeout(() => {
        this.props.onChange(this.props.id, value);
        this.setState({ updateTimeout: null });
      }, 300)
    });
  }

  renderInput() {
    const { id, className, ...props } = this.props;
    return (
      <input
        {...props}
        id={id}
        className={`input ${className}`}
        onChange={this.onChange}
        value={this.state.value}
      />
    );
  }

  renderFormGroup() {
    const { id, label } = this.props;
    return (
      <div className="form-group">
        {label && <label htmlFor={id}>{`${label}:`}</label>}
        {this.renderInput()}
      </div>
    );
  }

  render() {
    const { onlyInput } = this.props;
    return onlyInput ? this.renderInput() : this.renderFormGroup();
  }
}

// // Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Input.defaultProps = {
  className: '',
  type: 'text',
  id: null,
  label: null,
  onChange: null,
  placeholder: null,
  value: null,
  disabled: false,
  onlyInput: false,
  readOnly: false,
  required: false
};

Input.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  id: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  onlyInput: PropTypes.bool,
  readOnly: PropTypes.bool,
  required: PropTypes.bool
};

export default Input;

import React from 'react';
import Input from './input';


let inputValue = '';

function handleInput(event) {
  const { id, value } = event.target;
  inputValue = value;
}

describe('<Input />', () => {
  const wrapper = shallow(
    <Input
      id="input-id"
      label="Input label"
      placeholder="Type some text..."
      onChange={handleInput}
      value={inputValue}
      required
    />
  );

  // input without label and not required
  const wrapper2 = shallow(
    <Input
      id="input-id"
      placeholder="Type some text..."
      onChange={handleInput}
      value={inputValue}
    />
  );


  it('should verify Input structure', () => {
    // div
    expect(wrapper.find('div.form-group')).toHaveLength(1);
    expect(wrapper.children()).toHaveLength(2);
    // label
    expect(wrapper.find('label')).toHaveLength(1);
    expect(wrapper.childAt(0).children().node).toMatch('Input label:');
    expect(wrapper.childAt(0).prop('htmlFor')).toMatch('input-id');
    // input
    expect(wrapper.find('input')).toHaveLength(1);
    expect(wrapper.childAt(1).hasClass('input')).toBeTruthy();
    expect(wrapper.childAt(1).prop('id')).toMatch('input-id');
    expect(wrapper.childAt(1).prop('value')).toMatch('');
    expect(wrapper.childAt(1).prop('placeholder')).toMatch('Type some text...');
    expect(wrapper.childAt(1).prop('onChange')).toBeInstanceOf(Function);
    expect(wrapper.childAt(1).prop('required')).toBe(true);
  });


  it('should verify Input without label', () => {
    // div
    expect(wrapper2.children()).toHaveLength(1);
    // label
    expect(wrapper2.find('label')).toHaveLength(0);
    // input
    expect(wrapper2.find('input').prop('required')).toBe(false);
  });
});
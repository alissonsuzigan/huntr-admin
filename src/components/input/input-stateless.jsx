import React from 'react';
import PropTypes from 'prop-types';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Input({ id, label, className, placeholder, type, value, onChange, disabled, readOnly, required }) {
  return (
    <div className="form-group">
      {label && <label htmlFor={id}>{`${label}:`}</label>}
      <input
        id={id}
        className={`input ${className}`}
        value={value}
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        disabled={disabled}
        readOnly={readOnly}
        required={required}
      />
    </div>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Input.defaultProps = {
  className: '',
  disabled: false,
  id: '',
  label: '',
  placeholder: '',
  readOnly: false,
  required: false,
  type: 'text',
  value: ''
};

Input.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string
};

export default Input;

import React from 'react';
import Input from './input';


describe('<Input />', () => {

  describe('Validate input in form-group structure passing props', () => {
    const wrapper = shallow(
      <Input
        id="input-id"
        label="Input label"
        placeholder="Type some text..."
        onChange={jest.fn()}
        value="Default value"
        disabled
        readOnly
        required
      />
    );

    it('should validate component structure', () => {
      expect(wrapper.find('div')).toHaveLength(1);
      expect(wrapper.children()).toHaveLength(2);
      expect(wrapper.find('label')).toHaveLength(1);
      expect(wrapper.find('input')).toHaveLength(1);
    });

    it('should validate label element', () => {
      const label = wrapper.find('label');
      expect(label.children().node).toMatch('Input label:');
      expect(label.prop('htmlFor')).toMatch('input-id');
    });

    it('should validate input element', () => {
      const input = wrapper.find('input');
      expect(input.hasClass('input')).toBeTruthy();
      expect(input.prop('id')).toBe('input-id');
      expect(input.prop('placeholder')).toBe('Type some text...');
      expect(input.prop('type')).toBe('text');
      expect(input.prop('value')).toBe('Default value');
      expect(input.prop('onChange')).toBeInstanceOf(Function);
      expect(input.prop('disabled')).toBe(true);
      expect(input.prop('readOnly')).toBe(true);
      expect(input.prop('required')).toBe(true);
    });

  }); // Input in form-group structure


  describe('Validate onlyInput structure passing no props', () => {
    const wrapper = shallow(<Input onlyInput />);

    it('should validate component structure', () => {
      expect(wrapper.find('div')).toHaveLength(0);
      expect(wrapper.children()).toHaveLength(0);
      expect(wrapper.find('label')).toHaveLength(0);
      expect(wrapper.find('input')).toHaveLength(1);
    });

    it('should validate input element', () => {
      const input = wrapper.find('input');
      expect(input.hasClass('input')).toBeTruthy();
      expect(input.prop('id')).toBe(null);
      expect(input.prop('placeholder')).toBe(null);
      expect(input.prop('type')).toBe('text');
      expect(input.prop('value')).toBe(null);
      expect(input.prop('disabled')).toBe(false);
      expect(input.prop('readOnly')).toBe(false);
      expect(input.prop('required')).toBe(false);
    });

  });


  describe('Validate user interaction', () => {
    const changedValue = 'Changed value';
    const wrapper = shallow(
      <Input
        id="input-id"
        label="Input label"
        onChange={jest.fn()}
        value="Default value"
        required
        // onlyInput
      />
    );

    it('should validate input value changed', () => {
      expect(wrapper.state('value')).toBe('Default value');
      expect(wrapper.state('updateTimeout')).toBe(null);
      wrapper.find('input').simulate('change', {target: { value: changedValue }});
      expect(wrapper.state('value')).toBe(changedValue);
      expect(wrapper.state('updateTimeout')).not.toBe(null);
    });
  });

});
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputSlider from 'react-input-slider';
import cx from 'classnames';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class Time extends Component {
  constructor() {
    super();
    this.handleHours = this.handleHours.bind(this);
    this.handleMinutes = this.handleMinutes.bind(this);
  }

  handleHours(pos) {
    const m = this.props.moment;
    m.hours(parseInt(pos.x, 10));
    this.props.onChange(m);
  }

  handleMinutes(pos) {
    const m = this.props.moment;
    m.minutes(parseInt(pos.x, 10));
    this.props.onChange(m);
  }


  render() {
    const m = this.props.moment;

    return (
      <div className={cx('m-time', this.props.className)}>
        <div className="showtime">
          <span className="time">{m.format('HH')}</span>
          <span className="separater">:</span>
          <span className="time">{m.format('mm')}</span>
        </div>

        <div className="sliders">
          <div className="time-text">Horas:</div>
          <InputSlider
            className="u-slider-time"
            xmin={0}
            xmax={23}
            x={m.hour()}
            onChange={this.handleHours}
          />
          <div className="time-text">Minutos:</div>
          <InputSlider
            className="u-slider-time"
            xmin={0}
            xmax={59}
            x={m.minute()}
            onChange={this.handleMinutes}
          />
        </div>
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Time.defaultProps = {
  className: ''
};

Time.propTypes = {
  className: PropTypes.string,
  moment: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired
};

export default Time;

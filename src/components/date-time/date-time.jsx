import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
// Icons
import IconCalendar from 'react-icons/lib/md/date-range';
import IconClock from 'react-icons/lib/md/access-time';
import IconSave from 'react-icons/lib/md/check';
import IconClear from 'react-icons/lib/md/close';
// Local
import Calendar from './calendar';
import Time from './time';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class DateTime extends Component {
  constructor() {
    super();
    this.state = {
      tab: 0
    };
  }

  handleClickTab(event, tab) {
    event.preventDefault();
    this.setState({ tab });
  }

  render() {
    const { tab } = this.state;
    const { moment, className, onSave, ...props } = this.props;

    return (
      <div className={`date-time ${className}`} {...props}>
        <div className="date-time-options">
          <button
            type="button"
            className={cx('date-time-option', { 'is-active': tab === 0 })}
            onClick={event => this.handleClickTab(event, 0)}
          >
            <IconCalendar className="icon" /> Data
          </button>
          <button
            type="button"
            className={cx('date-time-option', { 'is-active': tab === 1 })}
            onClick={event => this.handleClickTab(event, 1)}
          >
            <IconClock className="icon" /> Horário
          </button>
        </div>

        <div className="date-time-tabs">
          <Calendar
            className={cx('date-time-tab', { 'is-active': tab === 0 })}
            moment={moment}
            onChange={this.props.onChange}
          />
          <Time
            className={cx('date-time-tab', { 'is-active': tab === 1 })}
            moment={moment}
            onChange={this.props.onChange}
          />
        </div>

        <div className="date-time-actions">
          <button
            type="button"
            className="btn btn-info im-btnx ion-closex icon"
            onClick={this.props.onClear}
          >
            <IconClear className="icon" /> Limpar
          </button>

          <button
            type="button"
            className="btn btn-info im-btnx ion-checkmarkx icon"
            onClick={this.props.onSave}
          >
            <IconSave className="icon" /> Salvar
          </button>
        </div>
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
DateTime.defaultProps = {
  className: '',
  defaultValue: ''
};

DateTime.propTypes = {
  defaultValue: PropTypes.string,
  className: PropTypes.string,
  moment: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  onClear: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};

export default DateTime;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import range from 'lodash/range';
import chunk from 'lodash/chunk';
// Icons
import IconArrowLeft from 'react-icons/lib/md/chevron-left';
import IconArrowRight from 'react-icons/lib/md/chevron-right';


// Day Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Day({ idx, week, day, className, ...props }) {
  const prevMonth = week === 0 && idx > 7;
  const nextMonth = week >= 4 && idx <= 14;
  const cls = cx({
    'prev-month': prevMonth,
    'next-month': nextMonth,
    'current-day': !prevMonth && !nextMonth && idx === day
  });
  return <td className={cls} {...props}>{idx}</td>;
}


// Calendar Component :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class Calendar extends Component {
  constructor() {
    super();
    this.prevMonth = this.prevMonth.bind(this);
    this.nextMonth = this.nextMonth.bind(this);
  }

  selectDate(idx, week) {
    const prevMonth = week === 0 && idx > 7;
    const nextMonth = week >= 4 && idx <= 14;
    const m = this.props.moment;
    m.date(idx);

    if (prevMonth) {
      m.subtract(1, 'month');
    }
    if (nextMonth) {
      m.add(1, 'month');
    }
    this.props.onChange(m);
  }

  prevMonth(event) {
    event.preventDefault();
    this.props.onChange(this.props.moment.subtract(1, 'month'));
  }

  nextMonth(event) {
    event.preventDefault();
    this.props.onChange(this.props.moment.add(1, 'month'));
  }

  render() {
    const m = this.props.moment;
    const day = m.date();
    const d1 = m.clone().subtract(1, 'month').endOf('month').date();
    const d2 = m.clone().date(1).day();
    const d3 = m.clone().endOf('month').date();
    const d1d2 = d1 - d2;
    const d3d2 = 42 - d3 - d2;
    const days = [].concat(
      range(d1d2 + 1, d1 + 1),
      range(1, d3 + 1),
      range(1, d3d2 + 1)
    );
    const weeks = m._locale._weekdaysShort;

    return (
      <div className={cx('m-calendar', this.props.className)}>
        <div className="toolbar">
          <button type="button" className="prev-month" onClick={this.prevMonth}>
            <IconArrowLeft className="icon" />
          </button>
          <span className="current-date">{m.format('MMMM YYYY')}</span>
          <button type="button" className="next-month" onClick={this.nextMonth}>
            <IconArrowRight className="icon" />
          </button>
        </div>

        <table>
          <thead>
            <tr>
              {weeks.map(week => <td key={week}>{week}</td>)}
            </tr>
          </thead>

          <tbody>
            {chunk(days, 7).map((row, week) => (
              <tr key={week.toString()}>
                {row.map(idx => (
                  <Day
                    key={idx.toString()}
                    idx={idx}
                    day={day}
                    week={week}
                    onClick={() => this.selectDate(idx, week)}
                  />
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Day
Day.defaultProps = {
  className: ''
};

Day.propTypes = {
  className: PropTypes.string,
  idx: PropTypes.number.isRequired,
  week: PropTypes.number.isRequired,
  day: PropTypes.number.isRequired
};

// Calendar
Calendar.defaultProps = {
  className: ''
};

Calendar.propTypes = {
  className: PropTypes.string,
  moment: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired
};

export default Calendar;

import React from 'react';
import PropTypes from 'prop-types';
import _map from 'lodash/map';
// Icons
import IconDetails from 'react-icons/lib/ti/document-text';
import IconEdit from 'react-icons/lib/md/edit';
import IconDelete from 'react-icons/lib/md/delete';
// Local
import { getExpireInformation } from '../../utils/date';
import statusIcon from '../../data/status-data.json';
import { textForField, textForkRule, joinValues } from '../../utils/dictionary';
import Status from '../status/status';
// SCSS
import './_table-rules.scss';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function TableBody({ tbodyData, serverTime }) {
  const onEdit = (id) => {
    window.location.hash = `/edit-rule/${id}`;
  };

  const onRemove = (id) => {
    window.location.hash = `/remove-rule/${id}`;
  };

  const renderExpireInformation = (startDate, endDate) => {
    const config = getExpireInformation(startDate, endDate, serverTime);
    return <span className={config.className}>{config.value}</span>;
  };

  const renderRows = row => (
    <tr key={row._id}>
      <td>
        {textForkRule(row.ruleBy)}<strong>{joinValues(row.ruleValue)}</strong>
        {textForField(row.field)}<strong>{joinValues(row.fieldValue)}</strong>
      </td>
      <td>{renderExpireInformation(row.startDate, row.endDate)}</td>
      <td><Status data={statusIcon} status={row.status} /></td>

      <td className="table-actions">
        <button
          className="btn btn-info-stroke only-icon"
          onClick={() => onEdit(row._id)}
          title="Detalhes regra"
        ><IconDetails />
        </button>
        <button
          className="btn btn-info-stroke only-icon"
          onClick={() => onEdit(row._id)}
          title="Editar regra"
        ><IconEdit />
        </button>
        <button
          className="btn btn-error-stroke only-icon"
          onClick={() => onRemove(row._id)}
          title="Remover regra"
        ><IconDelete />
        </button>
      </td>
    </tr>
  );

  return (
    <tbody>
      {_map(tbodyData, renderRows)}
    </tbody>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
TableBody.propTypes = {
  serverTime: PropTypes.number.isRequired,
  tbodyData: PropTypes.shape().isRequired
};

export default TableBody;

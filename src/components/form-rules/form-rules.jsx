import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Icons
import IconSave from 'react-icons/lib/md/check';
import IconBack from 'react-icons/lib/md/arrow-back';
// Redux
import { connect } from 'react-redux';
import { fetchRule, createRule, editRule } from '../../redux/actions';
// Local
import Input from '../input';
import Select from '../select';
import Range from '../range';
import InputTag from '../input-tag';
import InputDateTime from '../input-date-time';


// Select options :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const optionsStatus = [
  { value: '1', label: 'Ativo' },
  { value: '2', label: 'Inativo' },
  { value: '3', label: 'Pausado' }
];

const optionsField = [
  { value: 'id', label: 'Id do Produto' },
  { value: 'skuIds', label: 'Sku' },
  { value: 'categoryNames', label: 'Nome da categoria' },
  { value: 'departmentName', label: 'Nome do departamento' },
  { value: 'brandId', label: 'Id da marca' },
  { value: 'brandName', label: 'Nome da marca' },
  { value: 'sellerIds', label: 'Id do seller' },
  { value: 'sellerNames', label: 'Nome do seller' },
  { value: 'productCollectionIds', label: 'Id da coleção' }
];

const optionsRuleBy = [
  { value: 'term', label: 'Termo' }
];


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class FormRules extends Component {
  constructor(props) {
    super();
    this.state = {
      author: props.keycloak.userInfo.preferred_username,
      boost: '10',
      field: '',
      fieldValue: [],
      ruleBy: '',
      ruleValue: [],
      status: 1,
      startDate: '',
      endDate: ''
    };
    this.ruleId = props.match.params.id;
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setStateData = this.setStateData.bind(this);
    this.handleState = this.handleState.bind(this);
    this.gotoRules = this.gotoRules.bind(this);
  }

  componentWillMount() {
    if (this.ruleId) {
      this.props.fetchRule(this.ruleId).then(
        () => this.setStateData(this.props.huntr.rules[this.ruleId])
      );
    }
  }

  setStateData(data) {
    // Omit 'createDate' and 'updateDate' properties
    const { createDate, updateDate, ...stateData } = data;
    this.setState(stateData);
  }

  gotoRules() {
    this.props.history.push('/rules');
  }

  sortArrayData(data) {
    Object.values(data).forEach((item) => {
      if (item.constructor === Array) {
        item.sort();
      }
    }, this);
    return data;
  }

  // Handlers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  handleState(prop, value) {
    this.setState({ [prop]: value });
  }

  handleInput(event) {
    const { id, value } = event.target;
    this.setState({ [id]: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = this.sortArrayData({ ...this.state });

    if (this.ruleId) {
      this.props.editRule(this.ruleId, data, this.gotoRules);
    } else {
      this.props.createRule(data, this.gotoRules);
    }
  }

  // Render :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  render() {
    return (
      <form className="form-rules" autoComplete="off" onSubmit={this.handleSubmit}>
        <fieldset>
          <legend>Configurações da regra</legend>

          {this.ruleId && <Input id={this.ruleId} label="ID da regra" value={this.ruleId} readOnly />}

          <Select
            id="ruleBy"
            label="Regra por"
            dataOptions={optionsRuleBy}
            onChange={this.handleInput}
            value={this.state.ruleBy}
            required
          />

          <InputTag
            id="ruleValue"
            label="Valor"
            placeholder="Digite as regras"
            value={this.state.ruleValue}
            onChange={this.handleState}
          />

          <Select
            id="field"
            label="Selecione o campo"
            dataOptions={optionsField}
            onChange={this.handleInput}
            value={this.state.field}
            required
          />

          <InputTag
            id="fieldValue"
            label="Valores do campo"
            placeholder="Digite os valores"
            value={this.state.fieldValue}
            onChange={this.handleState}
          />

          <Select
            id="status"
            label="Status"
            dataOptions={optionsStatus}
            onChange={this.handleInput}
            value={this.state.status.toString()}
            required
          />

          <Range
            id="boost"
            label="Boost"
            min="-100"
            max="100"
            step="1"
            value={this.state.boost}
            onChange={this.handleState}
            required
          />

          <InputDateTime
            id="startDate"
            label="Data de início"
            onChange={this.handleState}
            value={this.state.startDate}
          />

          <InputDateTime
            id="endDate"
            label="Data de fim"
            onChange={this.handleState}
            value={this.state.endDate}
          />

          <div className="form-group right">
            <Link to="/rules" className="btn btn-info-stroke icon" title="Cancelar e voltar">
              <IconBack /> Cancelar
            </Link>
            <button className="btn btn-info icon" title="Cadastrar regra">
              <IconSave /> Salvar
            </button>
          </div>

        </fieldset>
      </form>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
FormRules.defaultProps = {
  id: ''
};

FormRules.propTypes = {
  id: PropTypes.string,
  fetchRule: PropTypes.func.isRequired,
  createRule: PropTypes.func.isRequired,
  editRule: PropTypes.func.isRequired,
  huntr: PropTypes.shape().isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string
    }).isRequired
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  keycloak: PropTypes.shape({
    userInfo: PropTypes.shape({
      preferred_username: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};


// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ huntr, keycloak }) => ({ huntr, keycloak });
export default connect(mapStateToProps, { fetchRule, createRule, editRule })(FormRules);

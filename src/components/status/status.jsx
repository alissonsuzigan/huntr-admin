import React from 'react';
import PropTypes from 'prop-types';
// Icons
import Active from 'react-icons/lib/fa/check';
import Inactive from 'react-icons/lib/fa/close';
import Schedule from 'react-icons/lib/fa/clock-o';
import Notfound from 'react-icons/lib/fa/exclamation';
import MdSnooze from 'react-icons/lib/md/snooze';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Status({ data, status }) {
  const components = {
    Active,
    Inactive,
    Schedule,
    Notfound,
    MdSnooze
  };
  const title = data[status].label;
  const style = { color: data[status].color };
  const Icon = components[data[status].icon];

  return (
    <i title={title}>
      <Icon style={style} />
    </i>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Status.propTypes = {
  status: PropTypes.number.isRequired,
  data: PropTypes.shape().isRequired
};

export default Status;

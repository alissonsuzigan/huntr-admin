import React from 'react';
import Status from './status';
// import statusData from '../../data/status-data.json';

var statusData = {
  "1": {
    "label": "Ativo",
    "icon": "Active",
    "color": "green"
  },
  "2": {
    "label": "Inativo",
    "icon": "Inactive",
    "color": "red"
  },
  "3": {
    "label": "Pausado",
    "icon": "MdSnooze",
    "color": "blue"
  },
  "4": {
    "label": "Excluído",
    "icon": "Notfound",
    "color": "yellow"
  },
  "5": {
    "label": "Agendado",
    "icon": "Schedule",
    "color": "yellow"
  }
}

describe('<Status />', () => {
  it('should verify Status structure', () => {
    const wrapper = shallow(<Status data={statusData} status={1} />);
    expect(wrapper.find('i')).toHaveLength(1);
    expect(wrapper.childAt(0)).toHaveLength(1);
  });

  it('should test Icon: Active', () => {
    const wrapper = shallow(<Status data={statusData} status={1} />);
    expect(wrapper.prop('title')).toMatch('Ativo');
    expect(wrapper.children().prop('style')).toEqual({'color': 'green'});
  });

  it('should test Icon: Inactive', () => {
    const wrapper = shallow(<Status data={statusData} status={2} />);
    expect(wrapper.prop('title')).toMatch('Inativo');
    expect(wrapper.children().prop('style')).toEqual({'color': 'red'});
  });

  it('should test Icon: MdSnooze', () => {
    const wrapper = shallow(<Status data={statusData} status={3} />);
    expect(wrapper.prop('title')).toMatch('Pausado');
    expect(wrapper.children().prop('style')).toEqual({'color': 'blue'});
  });

  it('should test Icon: Notfound', () => {
    const wrapper = shallow(<Status data={statusData} status={4} />);
    expect(wrapper.prop('title')).toMatch('Excluído');
    expect(wrapper.children().prop('style')).toEqual({'color': 'yellow'});
  });

  it('should test Icon: Schedule', () => {
    const wrapper = shallow(<Status data={statusData} status={5} />);
    expect(wrapper.prop('title')).toMatch('Agendado');
    expect(wrapper.children().prop('style')).toEqual({'color': 'yellow'});
  });
});

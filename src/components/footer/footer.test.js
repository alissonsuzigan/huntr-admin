import React from 'react';
import Footer from './footer';


describe('<Footer />', () => {
  const wrapper = shallow(<Footer />);

  test('should verify Footer structure', () => {
    expect(wrapper.find('footer#cart-footer.cart-footer')).toHaveLength(1);
    expect(wrapper.find('div.wrapper')).toHaveLength(1);
    expect(wrapper.children()).toHaveLength(1);
  });

  test('should verify footer-text structure', () => {
    expect(wrapper.find('p.footer-text')).toHaveLength(1);
    expect(wrapper.find('p.footer-text').text()).toBe('© 2017 Wal-Mart Stores, Inc.');
  });
});
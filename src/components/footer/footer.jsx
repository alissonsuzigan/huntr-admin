import React from 'react';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Footer() {
  return (
    <footer id="cart-footer" className="cart-footer">
      <div className="wrapper">
        <p className="footer-text">© 2017 Wal-Mart Stores, Inc.</p>
      </div>
    </footer>
  );
}

export default Footer;

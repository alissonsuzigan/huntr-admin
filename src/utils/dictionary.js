// Define texts for a specific field ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const textForField = (field) => {
  let fieldInfo;

  switch (field) {
    case 'id':
      fieldInfo = 'id do produto';
      break;

    case 'skuIds':
      fieldInfo = 'sku';
      break;

    case 'categoryNames':
      fieldInfo = 'nome da categoria';
      break;

    case 'departmentName':
      fieldInfo = 'nome do departamento';
      break;

    case 'brandId':
      fieldInfo = 'id da marca';
      break;

    case 'brandName':
      fieldInfo = 'nome da marca';
      break;

    case 'sellerIds':
      fieldInfo = 'id do seller';
      break;

    case 'sellerNames':
      fieldInfo = 'nome do seller';
      break;

    case 'productCollectionIds':
      fieldInfo = 'id da coleção';
      break;

    default:
      fieldInfo = '!campo desconhecido!';
  }
  return ` elevar o ${fieldInfo} com o(s) valor(es) `;
};


// Define texts for a specific rule :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const textForkRule = (ruleBy) => {
  switch (ruleBy) {
    case 'term':
      return 'Quando a busca for pelo(s) termo(s) ';

    default:
      return '!regra desconhecida!';
  }
};


// Join elements of an array into a string ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const joinValues = valueList => valueList.join(', ');


export {
  textForField,
  textForkRule,
  joinValues
};

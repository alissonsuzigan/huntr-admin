
// Format Value :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const formatValue = (number, plus = null) => {
  const value = plus ? number + plus : number;
  return (value <= 9 ? '0' : '') + value;
};


// Convert timestamp to date ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const timestampToDate = (timestamp) => {
  const date = new Date(timestamp);
  const day = formatValue(date.getDate());
  const month = formatValue(date.getMonth(), 1);
  const year = formatValue(date.getFullYear());
  const hour = formatValue(date.getHours());
  const min = formatValue(date.getMinutes());

  return `${day}/${month}/${year} - ${hour}:${min}`;
};

const diffTemplate = (strings, ...values) => {
  let string = '';

  string += values[0] > 0 ? `${values[0]} dia(s) ` : '';
  string += values[1] > 0 ? `${values[1]} hora(s) ` : '';
  string += values[2] > 0 ? `${values[2]} minuto(s)` : '';

  return string;
};

const diffDays = (start, end) => {
  const future = new Date(end);
  const now = new Date(start);

  const seconds = Math.floor((future - (now)) / 1000);
  let minutes = Math.floor(seconds / 60);
  let hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);

  hours -= days * 24;
  minutes = minutes - (days * 24 * 60) - (hours * 60);

  return diffTemplate`${days}${hours}${minutes}`;
};

// Return expire information ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const getExpireInformation = (startDate, endDate, now = new Date().getTime()) => {
  let value = 'expirado';
  let className = '';

  if (!endDate) {
    value = '∞';
    className = 'infinity';
  } else if (startDate > now) {
    value = diffDays(startDate, endDate);
    className = '';
  } else if (startDate <= now && endDate > now) {
    value = diffDays(now, endDate);
    className = '';
  }

  return {
    className,
    value,
    endDate
  };
};

export { timestampToDate, getExpireInformation };

import React from 'react';
import { render } from 'react-dom';

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';

// Keycloak
import Keycloak from 'keycloak-js';

// Local
import routes from './routes';
import reducers from './redux/reducers';

// Constants
import { HUNTR_EDIT, HUNTR_CREATE, HUNTR_READ_ONLY, HUNTR_DELETE } from './redux/constants';

// Redux Store configs
const store = createStore(reducers, applyMiddleware(promise, thunk));

const app = (
  <Provider store={store}>
    {routes}
  </Provider>
);

// render(app, document.querySelector('#huntr-admin'));

// Validate authentication by keycloak
const keycloak = Keycloak('keycloak.json');
keycloak.init({ onLoad: 'login-required', checkLoginIframe: false }).success((authenticated) => {
  if (authenticated) {
    keycloak.loadUserInfo().success((userInfo) => {
      const hasEditRole = keycloak.hasResourceRole(HUNTR_EDIT);
      const hasCreateRole = keycloak.hasResourceRole(HUNTR_CREATE);
      const hasReadRole = keycloak.hasResourceRole(HUNTR_READ_ONLY);
      const hasDeleteRole = keycloak.hasResourceRole(HUNTR_DELETE);
      const hasPermission = hasReadRole || hasEditRole || hasCreateRole || hasDeleteRole;

      store.getState().keycloak = {
        // hasPermission: true,
        hasPermission,
        userInfo
      };

      render(app, document.querySelector('#huntr-admin'));
    }).error(() => {
      throw new Error('Failed to Load User Info');
    });
  }
});

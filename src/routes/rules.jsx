import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Redux
import { connect } from 'react-redux';
import { fetchAllRules } from '../redux/actions';
// Local
import Table from '../components/table/table';
import TableRules from '../components/table-rules/table-rules';

const headerItems = ['Regra', 'Validade', 'Estado', 'Ações'];


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class Rules extends Component {
  componentDidMount() {
    this.props.fetchAllRules();
  }

  render() {
    const huntrRules = this.props.huntr.rules;
    const serverTime = this.props.huntr.serverTime;

    if (!huntrRules) {
      return <div>Carregando regras da busca...</div>;
    }

    return (
      <div className="all-rules-page">
        <h1 className="page-title">Regras cadastradas</h1>
        <Table
          // caption="Lista de regras cadastradas"
          className="table-striped table-hover table-rules"
          headerData={headerItems}
          tbodyData={huntrRules}
          tbodyTemplate={TableRules}
          serverTime={serverTime}
        />
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Rules.propTypes = {
  huntr: PropTypes.shape().isRequired,
  fetchAllRules: PropTypes.func.isRequired
};

// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ huntr }) => ({ huntr });
export default connect(mapStateToProps, { fetchAllRules })(Rules);

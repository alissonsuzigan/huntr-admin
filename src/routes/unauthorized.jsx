import React from 'react';
import PropTypes from 'prop-types';

// Redux
import { connect } from 'react-redux';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Unauthorized(props) {
  const { preferred_username, given_name } = props.keycloak.userInfo;
  return (
    <div>
      {/* eslint-disable camelcase */}
      <h3>Usuário não autorizado!</h3>
      <p>{given_name}, você não tem permissão de acesso neste sistema.</p>
      <p>
        {`Solicite o acesso do seu usuário '${preferred_username}' pelo `}
        <a href="https://eid.br.wal-mart.com/identity/faces/signin" target="_blank" rel="noopener noreferrer"> EID</a>.
      </p>
      {/* eslint-enable camelcase */}
    </div>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Unauthorized.propTypes = {
  keycloak: PropTypes.shape().isRequired
};

// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ keycloak }) => ({ keycloak });
export default connect(mapStateToProps)(Unauthorized);

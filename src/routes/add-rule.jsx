import React from 'react';
// Local
import FormRules from '../components/form-rules/form-rules';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function AddRule(props) {
  return (
    <div>
      <h1 className="page-title">Cadastrar regra</h1>
      <FormRules {...props} />
    </div>
  );
}

export default AddRule;

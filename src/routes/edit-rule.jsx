import React from 'react';
// Local
import FormRules from '../components/form-rules/form-rules';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function EditRule(props) {
  return (
    <div>
      <h1 className="page-title">Editar regra</h1>
      <FormRules {...props} />
    </div>
  );
}

export default EditRule;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Icons
import IconDelete from 'react-icons/lib/md/delete';
import IconBack from 'react-icons/lib/md/arrow-back';
// Redux
import { connect } from 'react-redux';
import { fetchRule, deleteRule } from '../redux/actions';
// Local
import { timestampToDate } from '../utils/date';
import { textForField, textForkRule, joinValues } from '../utils/dictionary';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class RemoveRule extends Component {
  constructor() {
    super();
    this.onRemove = this.onRemove.bind(this);
  }

  componentDidMount() {
    this.props.fetchRule(this.props.match.params.id);
  }

  onRemove() {
    this.props.deleteRule(this.props.match.params.id, () => {
      this.props.history.push('/rules');
    });
  }

  render() {
    const rule = this.props.huntr.rules[this.props.match.params.id];

    if (!rule) {
      return <div>loading...</div>;
    }

    return (
      <div className="remove-rule-page">
        <h2 className="page-title">Confirma a exclusão da regra?</h2>

        <table className="table table-bordered">
          <caption>Informações da regra:</caption>
          <tbody>
            <tr>
              <td className="table-info">Descrição da regra:</td>
              <td>
                {textForkRule(rule.ruleBy)}<strong>{joinValues(rule.ruleValue)}</strong>
                {textForField(rule.field)}<strong>{joinValues(rule.fieldValue)}</strong>
              </td>
            </tr>
            <tr>
              <td className="table-info">Autor:</td>
              <td>{rule.author}</td>
            </tr>
            <tr>
              <td className="table-info">Data de criação:</td>
              <td>{timestampToDate(rule.createDate)}</td>
            </tr>
          </tbody>
        </table>

        <div className="form-group right">
          <Link to="/rules" className="btn btn-error-stroke icon" title="Cancelar e voltar">
            <IconBack />Cancelar
          </Link>
          <button className="btn btn-error icon" onClick={this.onRemove} title="Remover regra!">
            <IconDelete />Excluir
          </button>
        </div>

      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
RemoveRule.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  huntr: PropTypes.shape().isRequired,
  match: PropTypes.shape().isRequired,
  fetchRule: PropTypes.func.isRequired,
  deleteRule: PropTypes.func.isRequired
};

// Redux settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ huntr }) => ({ huntr });
export default connect(mapStateToProps, { fetchRule, deleteRule })(RemoveRule);

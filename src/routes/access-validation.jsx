import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


class AccessValidation extends PureComponent {
  componentDidMount() {
    this.valideteAccess();
  }

  componentDidUpdate() {
    this.valideteAccess();
  }

  valideteAccess() {
    const { pathname } = this.props.location;
    const { hasPermission } = this.props.keycloak;

    if (!hasPermission && pathname !== '/unauthorized') {
      this.props.history.push('/unauthorized');
    } else if (hasPermission && pathname === '/unauthorized') {
      this.props.history.push('/rules');
    }
  }

  render() {
    return null;
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
AccessValidation.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,

  keycloak: PropTypes.shape({
    hasPermission: PropTypes.bool.isRequired
  }).isRequired,

  history: PropTypes.shape({
    push: PropTypes.string.isRequired
  }).isRequired
};

// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ keycloak }) => ({ keycloak });
export default connect(mapStateToProps)(AccessValidation);

import _mapKeys from 'lodash/mapKeys';
import _omit from 'lodash/omit';
import { FETCH_ALL_RULES, FETCH_RULE, CREATE_RULE, EDIT_RULE, DELETE_RULE } from './actions';


// Huntr Reducer ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Huntr(state = {}, action) {
  let rules;

  switch (action.type) {
    case FETCH_ALL_RULES:
      rules = _mapKeys(action.payload.data.rules, '_id');
      return { ...state, rules, serverTime: action.payload.data.serverTime };

    case FETCH_RULE:
      rules = { ...state.rules, [action.payload.data._id]: action.payload.data };
      return { ...state, rules };

    case CREATE_RULE:
      rules = { ...state.rules, [action.payload.data._id]: action.payload.data };
      return { ...state, rules };

    case EDIT_RULE:
      rules = { ...state.rules, [action.payload.data._id]: action.payload.data };
      return { ...state, rules };

    case DELETE_RULE:
      // action.payload is the ID of the rule
      return _omit(state.rules, action.payload);

    default:
      return state;
  }
}

export default Huntr;

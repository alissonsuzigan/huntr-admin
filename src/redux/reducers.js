import { combineReducers } from 'redux';
import Huntr from './reducer-huntr';
import keycloak from './keycloak';


// Combine Reducers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const reducers = combineReducers({
  huntr: Huntr,
  keycloak
});

export default reducers;

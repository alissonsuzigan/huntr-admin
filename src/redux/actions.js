import axios from 'axios';
import { HUNTR_API, AUTH, ALL_RULES, RULE, CREATE, EDIT, DELETE } from './constants';

const config = {
  auth: AUTH
};

// Actions Types ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const FETCH_ALL_RULES = 'FETCH_ALL_RULES';
export const FETCH_RULE = 'FETCH_RULE';
export const CREATE_RULE = 'CREATE_RULE';
export const EDIT_RULE = 'EDIT_RULE';
export const DELETE_RULE = 'DELETE_RULE';
export const FETCH_RULES_ERROR = 'FETCH_RULES_ERROR';


// Actions Creators :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// Fetch All Rules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const fetchAllRules = () => {
  const url = `${HUNTR_API}${ALL_RULES}`;
  const request = axios.get(url, config);

  return {
    type: FETCH_ALL_RULES,
    payload: request
  };
};

// Featch a Rule ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const fetchRule = (id) => {
  const url = `${HUNTR_API}${RULE}${id}`;
  const request = axios.get(url, config);

  return {
    type: FETCH_RULE,
    payload: request
  };
};

// Add Rule :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const createRule = (data, success) => {
  const url = `${HUNTR_API}${CREATE}`;
  // using redux-thunk
  return (dispatch) => {
    axios.post(url, data, config)
      .then(response => dispatch({
        type: CREATE_RULE,
        payload: response
      }))
      .then(() => success());
  };
};

// Edit Rule :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const editRule = (id, data, success) => {
  const url = `${HUNTR_API}${EDIT}${id}`;
  // using redux-thunk
  return (dispatch) => {
    axios.patch(url, data, config)
      .then(response => dispatch({
        type: EDIT_RULE,
        payload: response
      }))
      .then(() => success());
  };
};

// Remove Rule ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const deleteRule = (id, callback) => {
  const url = `${HUNTR_API}${DELETE}${id}`;
  axios.patch(url, null, config).then(() => callback());

  return {
    type: DELETE_RULE,
    payload: id
  };
};

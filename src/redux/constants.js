// API Constants ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// API url path
export const HUNTR_API = window.location.hostname === 'localhost'
  ? 'http://vip-search-huntr-api.qa.vmcommerce.intra' : window.location.origin;

// Find all rules | .../ | get
export const ALL_RULES = '/rules';

// Find a rule by id | .../rule/{id} | get
export const RULE = '/rule/';

// Create a new rule | .../rule | post
export const CREATE = '/rule';

// Update an existent rule | .../rule/{id} | patch
export const EDIT = '/rule/';

// Remove a rule by id | .../rule/delete/{id} | patch
export const DELETE = '/rule/delete/';

// Auth info
export const AUTH = { username: 'admin', password: 'runt3R2017' };

// Rules in Keycloak
export const HUNTR_EDIT = 'huntr_admin_edit';
export const HUNTR_CREATE = 'huntr_admin_create';
export const HUNTR_DELETE = 'huntr_admin_delete';
export const HUNTR_READ_ONLY = 'huntr_admin_read_only';

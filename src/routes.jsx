import React from 'react';
import { HashRouter, Route } from 'react-router-dom';

// Header :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
import Header from './components/header';
import headerData from './data/header-data.json';
import Footer from './components/footer';

// Routes :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
import AccessValidation from './routes/access-validation';
import Rules from './routes/rules';
import AddRule from './routes/add-rule';
import EditRule from './routes/edit-rule';
import RemoveRule from './routes/remove-rule';
import Unauthorized from './routes/unauthorized';

// Global style :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
import './styles/global.scss';
import './routes/_routes.scss';


// Router structure :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export default (
  <HashRouter>
    <div id="app">
      <Header data={headerData} />
      <main id="main" className="main">
        <Route exact path="/" component={Rules} />
        <Route path="/rules" component={Rules} />
        <Route path="/add-rule" component={AddRule} />
        <Route path="/edit-rule/:id" component={EditRule} />
        <Route path="/remove-rule/:id" component={RemoveRule} />
        <Route path="/unauthorized" component={Unauthorized} />
        <Route path="/" component={AccessValidation} />
      </main>
      <Footer />
    </div>
  </HashRouter>
);

const config = {
  huntr_api: null,
  keycloak_url: null,
  keycloak_realm: null,
  keycloak_client_id: null,
  keycloak_secret: null
};

export default config;
